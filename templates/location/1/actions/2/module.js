/**
 * Created by a116918 on 19/12/2014.
 */
//Get a reference to the injector to get a hold on the URLFinderService, we need to do this because registerController
    // is out of Angular IoC capabilities and we need the service to create the controller's name.
var injector =angular.element(document.querySelector( "#appRoot")).injector();
var URLFinderService=injector.get("URLFinderService");
var ctrlName=URLFinderService.generateControllerName(1,2);
angular.module('myApp').registerController(ctrlName,['$scope',function($scope){
    $scope.salute="I`m in the second template";

    $scope.sayHello=function(){
        alert('Hello from a dynamic controller');
    }

}])

